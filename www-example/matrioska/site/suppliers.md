---
title: Buy a matrioska
...

These sales fund matrioskas
===========================

In the past, there was Toys R Us.

Alas, they no longer exist, but you can probably find a local toy store that
sells matrioska.

Matrioska need a home. They are manufactured in large number, and they all need
a loving companion!