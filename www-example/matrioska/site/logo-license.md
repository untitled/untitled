---
title: matrioskas licenses
...

Having build instructions under free licenses makes sure that your
matrioska is free hardware.

However you don't need any license to modify your matrioska hardware.